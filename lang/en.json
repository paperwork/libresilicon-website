{ "lang": "en"
, "title": "Libre Silicon"
, "menu": "Menu"
, "language": "Language"
, "about": "About"
, "news": "News"
, "members": "Members"
, "whitepaper": "Whitepaper"
, "contact": "Contact"
, "homeHeading": "Libre Silicon"
, "homeSubtitle": "Free semiconductors for everyone"
, "homeContent": "We develop a free (as in freedom, not as in free of charge) and open source semiconductor manufacturing process standard, including a full mixed signal PDK, and provide a quick, easy and inexpensive way for manufacturing. No NDAs will be required anywhere to get started, making it possible to build the designs in your basement if you wish so. We are aiming to revolutionize the market by breaking through the monopoly of proprietary closed source manufacturers!"
, "aboutButton": "The idea"
, "homeButton": "Get more info"
, "startButton": "Getting started"
, "processButton": "The process"
, "aboutTitle": "Opening the market"
, "securityTitle": "Auditable chips solving security concerns"
, "securityContent": "Since at least the introduction of the Intel Management Engine and the emergence of Spectre and Meltdown, the concerns about the trustworthiness and security of CPUs in our computers, smartphones and other devices is raising. By providing a fully open process and technology node which allows to publish the entire layout, the doors for entirely free CPU platforms are being pushed open which allow for a full security audit down to the transistor level. Which means increased assurance of security via peer review and no more hidden hardware backdoors for intelligence agencies."
, "processTitle": "Leveling the playfield"
, "processContent": "Today, the field of free and open-source silicon is very uneven. SMEs, individual FOSHW developers and hobbyists have no chance to implement their designs for practical purposes except for FPGAs, with no provision at all for analog, mixed-signal or high-performance, low-power, space-constrainted applications. As for \"Open\" chips, the privilege of IP integation, \"the final pull\" is in the hands of their sponsors, barring users from benefiting from the advantages of their FOSHW nature. On the other end of the pipe, Big-tech corporations embracing the \"open-source is out-source\" paradigm are siphoning the effort of developers for gratis into commercial products. By providing unfettered and viable access to small players to semiconductor manufacturing, we aim to turn \"open-source silicon\" into true Free and Open-Source LibreSilicon!"
, "aboutContent": "Today there is no inexpensive and easy way for SMEs, startups and hobbyists to develop their own ASICs and bringing them to the market. All the silicon manufacturing nowadays is in the hands of a few big companies which play their monopoly of lone capability of manufacturing microchips by charging around 2'000 USD per prototype and letting an engineer wait for months until she/he can test her/his design physically. Additionally the EDA tools for designing chips are unaffordable for most start ups. We intend to change that!"
, "collaborationTitle": "A collaboration platform"
, "collaborationContent": "By using a blockchain solution in order to track the added value from the work of every contributor, without any human intervention required, it is ensured that everyone is being payed a fair price for the work and time she or he has invested into developing an ASIC or IP block. Manufacturing potential can be reported on a market place where customers can put up orders. The result will be something like a crypto exchange just without speculation, a way for people who want to buy chips and people with a basement containing a clean room to find each other. This becomes possible with our free semiconductor manufacturing process standard which allow reproducible results with varying setups."
, "reproducibilityTitle": "Global reproduciblity"
, "reproducibilityContent": "In order to democratize the semiconductor market, a standardized free manufacturing process needs to be introduced in order to easily exchange designs and silicon-verify them anywhere on the globe. By specifying the target parameters of test structures we provide, required for supporting the LibreSilicon technology, it becomes possible to design and test any ASIC (CMOS *and* analog!) somewhere in Tokyo and then download the design from GitHub and manufacture it anywhere else in the world. That's what we're doing."
, "visionTitle": "Our Vision and Mission"
, "visionText": [
	"Our vision is a world where the \"Silicon Barrier\" does not exist. Access to integrated circuit technology is as commonplace as printed circuit board is, and not only for the top corporations. Knowledge of integrated circuit design is the part of the basic skills of an electrical engineer not only because of its relevance, but also because of its everyday usefulness. High-quality FOSS electronic design automation software is available. Process Design Kits, the information needed to design for a particular technology node, are publicly available. GDSII is an everyday counterpart of Gerber files. The paradigm of large-scale integration penetrates the product or project designs of companies, being large or small, SMEs, start-ups, and individual hobbyists and amateurs.",
        "Deciding what and how is made in CPUs and SoCs of computers, smartphones, routers and other everyday computing devices is not a privilege of a few big corporations. The freedoms defined in relation to free software are valid to computer hardware: users are permitted to study, or if one decides so, modify them. Trust towards digital technology is rooted in complete transparency. CPU design is not one-size-fits-all, custom CPUs are commonplace.",
	"Having these custom integrated circuits manufactured is affordable, providing a comparable economic performance over discrete or programmable-logic realization even down to the smallest volumes. The required capability of realization of unique ICs is provided by a new branch of the semiconductor industry: fabs, ranging from garage enterprises to industrial-scale ones, distributed all around the globe, serving local and/or global demand of custom IC fabrication. As for their clients, everyone is invited, even individuals, with no minimum order quantities, nondisclosure agreements or demands of excessive legal paperwork. Ordering a low-volume custom IC run is as easy and straightforward as ordering a PCB. The common denominator between these fabs is a set of open-source technology nodes that provide design portablity."]
, "visionSubTexts": [
	{ "title": "What needs to be changed ..." , "paragraphs": [
	"One may say, the vision outlined before is the complete opposite of the current situation, and one is right. There are many areas that need to be addressed, from paradigm change and education to the availablity of viable free CPU designs and of semiconductor manufacturing services.",
	"First and foremost, the attitude towards semiconductor industry in general needs a shift: it is impossible to reach the goal as long as no-one believes that IC fabrication **_can_** be more accessible than it is today. Also, a **_paradigm shift_** is necessary: that designers consider integrated implementation instead of realization using a PCB with catalog components as a viable alternative. Besides that, the specific **_knowledge_** and skills on IC design, including analog, digital and system-level aspects, has to be included in education of engineers, both via institutional and autodidactic paths.",
	"In addition to these, breaking the \"Silicon Barrier\" is not possible without the proper tooling. Design of integrated circuits is a speciality field within electrical engineering, therefore specialized **_EDA tools_** are necessary, that have the maturity and quality to be used for practical work. This includes, among others, circuit simulation, layout, DRC, LVS, and parasitic extraction tools, electromigration, signal and power integrity checkers, and many more. Unfortunately, current software implementing these functions are either commercial products with pricetags targeted at big-tech, or open-source software with limited functionality and maturity.",
	"Of course, all of these can work only if there is a rationale to invest effort into them, which mandates the availablity of useful **_designs_** and a way to exchange them. One of the most anticipated benefits of LibreSilicon is the transparency, customizablity and trustworthyness of the CPUs and SoCs it provides, that means it is imperative for success that high-quality open-source digital processor core designs exist. Also, specialized applications will need purpose-tailored digital cores, as well as analog blocks and the primitives to form them. Of course, the integration of a digital core into a real chip requires many non-digital components, like bonding pads, ESD protection, oscillators, PLLs, nonvolatile memories, various physical interface circuits and manufacturing-related features.",
	"An important, if not the most important prerequsite of the change is the availablity of a set of **_free technology nodes_** on which these designs can be realized. The range and feature set of these nodes needs to be diverse, with deep-submicrometer nodes available for dense digital designs, larger feature-size nodes for mixed-signal and analog circuits, and specialized options for niche applications like power management, high-voltage, RF, and so on. The free and open nature of these nodes is imperative, as it enables the unfettered availablity of PDKs to designers, and allows massive adoption by fabs, which is a prerequsite for design portablity and supply-chain security.",
	"These nodes, however, carry no practical value alone. In order to be useful, it is needed that the nodes are actually **_being manufactured._** In practice, this may be achieved by adoption of the nodes by existing fabs, or by the emergence of new manufacturers tailored for them, or by both. In all cases, the geographically diverse availablity of independent manufacturing vendors is desirable, as it increases the resilience of the supply chain and prevents monopolization, thus enables affordable access via competition. It has to be understood that many FOSHW developers are motivated into developing IP cores to solve real-life problems. Since, as stated before, the availablity of high-quality IP cores is imperative, accessiblity of these services to small customers is an important aspect. Therefore, and for the true provision of user-customizablity of FOSHW designs, it is paramount to find, implement and encourage the adoption of **_novel business models and behaviors_** that allow small players to benefit from LibreSilicon. This includes not only a change in the now-current attitude of fabs slamming the door at the customer below one million units, but also a departure from optimizing manufacturing processes to massively large volumes. The latter is also important to ensure that low-volume production is not only accessible, but also affordable. This involves re-thinking of economies of scale, introduction of new ways to organize manufacturing operations, and the development and introduction of **_novel mask-less lithography technologies,_** that can meet the resolution and troughput requirements of cost-effective production both at large and small scale."
       ]},
       { "title": "... and how will we make it happen?", "paragraphs": [
	    "We aim to take an active role in driving forward the change and reaching the objectives outlined before. Considering the fact that the free and open-source silicon ecosystem is growing explosively since the second half of the 2010s, many of the objectives are addressed already by others. In these cases, we want to support their effort and adapt their results, without aiming at realizing the same goal in a different way, thus preventing the fragmentation of the ecosystem. In other topics, however, there is either no progress or the progress is not going in the desirable direction. In these cases, we take a leading role, using the fullest possible extent of our expertise, commitment and resources. Our focus topics, without particular order, are the following:",
	    "* **_Develop free technology nodes,_** including manufacturing recipes, test structures, primitive devices, logic, padring, analog and other libraries, characteristization methodology, and PDK support.",
	    "* **_Drive the adoption of these nodes,_** by providing the necessary documentation, consulting and technical support. We also aim to set the example to follow by being early adopters of our own technology nodes, including the provision of actual manufacturing service.",
	    "* **_Elaborate novel business models_** to support the emergence of universal and affordable access to semiconductor technologies. This includes among others, novel manufacturing operation organization concepts, troughput optimization for low-volume or high-volume production, and the introduction of new, connected manufacturing concepts and equipment.",
	    "* **_Develop novel manufacturing equipment_** enabling low-volume-capable, high-flexiblity manufacturing, with primary focus on maskless lithographic technologies.",
	    "In addition to these efforts, we also aim at providing valuable contribution in the efforts of other with a shared goal, including:",
	    "* **_Drive the necessary paradigm shift_** by advocating for free and open-source silicon solutions in real-life applications",
	    "* **_Support the education on IC design-related skillset_** by taking active role in the formation of training materials, supporting and organizing events like Hackathons or workshops",
	    "* **_Support the development of EDA tools_** by advising on, and contributing to, the developemnt of FOSS IC design software",
	    "* **_Increase the scope and quality of available free silicon IP libraries_** by developing our own libraries and contributing to the development of others"
       ] },
       { "title": "Our roadmap", "paragraphs": [
            "As of 2021, our tentative roadmap is the following:",
	    "- Eastablishment of LibreSilicon Foundation in Europe",
	    "- Finish LS1U technology node (incl. SPICE parameter extraction)",
	    "- Development of ESD protection and pad cell library for LS1U",
	    "- Development of a maskless lithography stepper, initially at least for 50um MFS, later for LS1U",
	    "- Tech Demo: LS555 (design, MLL fabrication, testing)",
	    "- Digital cell library for 1u MFS + digital tools (may be parallel to LS1U/LS555)",
	    "- Tech demo II: 8-bit Arduino-compatible MCU",
	    "...",
	    "- Libresilicon Fabrication Service opens in EU",
	    "...",
	    "- Development of LS130 technology node (incl. MLL capablity)",
	    "- Tech demo III: 130nm System-on-Chip",
	    "...",
	    "- Development of sub-100nm technology node (incl. MLL capablity)",
	    "- Tech demo IV: SOC made on LibreSilicon runs GNU/Linux",
	    "..."
       ] }
]
, "gitUrlQtFlow": "https://github.com/libresilicon/qtflow"
, "wikiUrlQtFlow": "https://murmur.libresilicon.com/foshardware/wiki/QtFlow"
, "rpmsUrlQtFlow": "https://download.opensuse.org/repositories/home:/leviathanch:/qtflow-debian"
, "debsUrlQtFlow": "https://download.opensuse.org/repositories/home:/leviathanch:/qtflow-debian"
, "repoUrlStandardCells": "https://github.com/chipforge/StdCellLib"
, "repoUrlProcessSpec": "https://github.com/libresilicon/process"
, "mailingListUrl": "https://list.libresilicon.com/mailman/listinfo/libresilicon-developers"
, "newsTitle": "News"
, "newsItems":
  [ 
   { "title": "Successful tapeout of the Danube with Global Foundries"
    , "description": [
	    "12/21/2023: The first successful tapeout of the new autogenerated process",
		"verification wafer Danube River (https://wiki.libresilicon.com/index.php?title=Danube_River)"
    	]
  },
  { "title": "Congress talk at the 35C3"
    , "description": [
	    "11/14/2018: There will be a 1.5 hour talk about this project at Chaos Communication Congress 35c3."
    	]
    },
    { "title": "Lightning talk at the 34c3"
    , "description": [
	    "12/16/2017: There will be a lightning talk about this project at Chaos Communication Congress 34c3."
    	]
    },
    { "title": "Hackathon at the C3D2"
    , "description": [
	    "Free semiconductors for everyone",
	    "from: 2018-05-19 Sat 12h",
	    "until: 2018-05-21 Mon OpenEnd",
	    "Zentralwerk Riesaer Str. 32, 01127 Dresden",
	    "https://www.openstreetmap.org/node/4709923560"
	 ]
    }
  ]
, "lsaMembers": "LSA Members"
, "lsaBackers": "LSA Backers"
, "readWhitepaperHere": "Read our whitepaper here"
, "viewPdf": "VIEW PDF"
, "additionalDocs": "Additional documents"
, "theProcess": "The process"
, "contactUs": "Contact us"
, "gettingStarted": "Getting started"
}
