<!DOCTYPE html>
<html lang="{{lang}}">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="LibreSilicon attempts to break the microchip monopoly. By developing a free and open source process design kit + tool stack we enable the fabrication of truly open hardware.">
    <meta name="author" content="Lanceville Technology Group Ltd">

    <title>{{title}}</title>

    <link href="news.rss" rel="alternate" type="application/rss+xml" title="Libre Silicon">
    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="vendor/bootstrap-languages/languages.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

    <!-- Social Buttons -->
    <link href="css/social.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="css/creative.css" rel="stylesheet">

    <!-- CSS customization -->
    <link href="css/custom.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top">

    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" id="menu-toggle" data-toggle="collapse" data-target="#menu-nav">
                    <span class="sr-only">Toggle navigation</span> {{menu}} <i class="fa fa-bars"></i>
                </button>
                <button type="button" class="navbar-toggle collapsed" id="lang-toggle" data-toggle="collapse" data-target="#lang-nav">
		    <span class="sr-only">Toggle navigation</span> {{language}} <span class="lang-sm"></span>
                </button>

	</div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="menu-nav">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll" href="#about">{{about}}</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#news">{{news}}</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#members">{{members}}</a>
                    </li>
                    <!--<li>
                        <a class="page-scroll" href="#whitepaper">{{whitepaper}}</a>
                    </li>-->
                    <li>
                        <a class="page-scroll" href="#contact">{{contact}}</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <header>

<div id="landing-carousel" class="carousel slide" data-ride="carousel">

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
      <img src="img/carousel/01.jpg" />
    </div>

    <div class="item">
      <img src="img/carousel/02.jpg" />
    </div>

    <div class="item">
      <img src="img/carousel/03.jpg" />
    </div>

    <div class="item">
      <img src="img/carousel/04.jpg" />
    </div>

    <div class="item">
      <img src="img/carousel/05.jpg" />
    </div>

    <div class="item">
      <img src="img/carousel/06.jpg" />
    </div>

    <div class="item">
      <img src="img/carousel/07.jpg" />
    </div>

  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#landing-carousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#landing-carousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

  <div class="header-content readability">
      <h1 id="homeHeading">{{homeHeading}}</h1>
      <h2 id="homeSubtitle">{{homeSubtitle}}</h3>
      <p>{{homeContent}}</p>
      <a href="#about" class="btn btn-primary btn-xl page-scroll">{{aboutButton}}</a>
      <a href="#whitepaper" class="btn btn-primary btn-xl page-scroll">{{homeButton}}</a>
      <a href="#getstarted" class="btn btn-primary btn-xl page-scroll">{{startButton}}</a>
      <!--<a href="#theprocess" class="btn btn-primary btn-xl page-scroll">{{processButton}}</a>-->
  </div>

    </header>

    <section id="about">
        <div class="container" style="padding-top: 10%">
            <div class="row">
                <div class="col-lg-6 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-bank text-primary sr-icons"></i>
                        <h3 class="hc">{{aboutTitle}}</h3>
			<p>{{aboutContent}}</p>
                    </div>
                </div>
		<div class="col-lg-6 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-cubes text-primary sr-icons"></i>
                        <h3 class="hc">{{securityTitle}}</h3>
                        <p>{{securityContent}}</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-cloud text-primary sr-icons"></i>
                        <h3 class="hc">{{reproducibilityTitle}}</h3>
                        <p>{{reproducibilityContent}}</p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-balance-scale text-primary sr-icons"></i>
                        <h3 class="hc">{{processTitle}}</h3>
                        <p>{{processContent}}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section id="vision">
        <div class="container text-left">
            {{large_content}}
        </div>
    </section>

    <section id="news">
        <div class="container text-center">
            <h1>{{newsTitle}} <a href="http://libresilicon.com/news_{{lang}}.rss" rel="alternate" type="application/rss+xml"><i class="fa fa-rss sr-icons"></i></a>
            </h1>
            {% for item in newsItems %}
            <div class="row" style="margin-top: 50px;">
                <div class="col-lg-12 col-md-12 text-center">
	 	    <h3 class="section-heading">{{ item.title }}</h3>
                    {{ item.description }}
                </div>
            </div>
            {% endfor %}
        </div>
    </section>

    <section id="members">
        <div class="container text-center">
            <h1>Members</h1>

<div class="row" style="margin-top: 5em">

  <div class="column">
    <div class="card">
      <img src="img/dave.jpg" alt="Dave" style="width:100%">
      <div class="card-container">
        <h2>David Lanzendörfer</h2>
        <p class="title">Process Design</p>
        <p>Studied electrical engineering but realized that there were no interesting jobs to be done in Switzerland. So he ventured out to China in order find his fortune there. In this new world he found all the thrilling engineering problems, he ever wanted to solve, and now tackles the final frontier: Semiconductor manufacturing.</p>
        <p>david dot lanzendoerfer at libresilicon dot com</p>
      </div>
      <div class="share">
          <a class="twitter" href="https://twitter.com/leviathanch" target="blank"><i class="fa fa-twitter"></i></a>
          <a class="facebook" href="https://www.facebook.com/david.lanzendoerfer" target="blank"><i class="fa fa-facebook"></i></a>
      </div>
    </div>
  </div>

  <!--<div class="column">
    <div class="card">
      <img src="img/andreas.jpg" alt="Andreas" style="width:100%">
      <div class="card-container">
        <h2>Andreas Westerwick</h2>
        <p class="title">Compiler Design</p>
        <p>Programmer with 15 years experience. Computer science and math enthusiast. Strongly invested in decentralized systems. Haskell novice.</p>
        <p>andreas dot westerwick at libresilicon dot com</p>
      </div>
    </div>
  </div>-->

  <div class="column">
    <div class="card">
      <img src="img/philipp.jpg" alt="Philipp" style="width:100%">
      <div class="card-container">
        <h2>Philipp Gühring</h2>
        <p class="title">Software Design</p>
        <p>Software developer for 25 years. Security and Cryptography enthusiast.</p>
        <p>philipp dot guehring at libresilicon dot com</p>
      </div>
    </div>
  </div>

  <div class="column">
    <div class="card">
      <img src="img/hagen.jpg" alt="Hagen" style="width:100%">
      <div class="card-container">
        <h2>Hagen Sankowski</h2>
        <p class="title">Digital Design</p>
        <p>In business for 20 years, worked for a Samsung subsidiary, later freelancing for Intel Mobile Communication, Infineon Chipcard, Globalfoundries and many others as ASIC Designer.</p>
        <p>hsank at posteo dot de</p>
      </div>
      <div class="share">
          <a class="linkedin" rel="noreferrer" href="https://de.linkedin.com/in/hagen-sankowski-14300910" target="blank"><i class="fa fa-linkedin"></i></a>
      </div>
    </div>
  </div>

  <div class="column">
    <div class="card">
      <img src="img/eeger.jpg" alt="Ferenc" style="width:100%">
      <div class="card-container">
        <h2>Ferenc Éger</h2>
        <p class="title">Analog Design</p>
        <p>Electrical engineer, currently in the automotive sector. Previously worked at Microchip Technology, and on custom-order system development projects (both HW and SW) since 2010. Studied microelectronics manufacturing and design at Budapest University of Technology.</p>
        <p>eegerferenc at gmail dot com</p>
      </div>
    </div>
  </div>

</div>


        </div>
    </section>

    <section id="getstarted">
        <div class="container">
            <div>
                <div class="col-xs-12 text-center">
                    <h2 class="section-heading">{{gettingStarted}}</h2>
                </div>
                <div class="col-lg-3 col-md-3 text-center">
                    <a rel="noreferrer" href="https://gitlab.libresilicon.com/explore/projects" target="_blank">
                        <img width="120px" style="margin-top: 20px;" src="img/gitlab-logo.png">
                        <p><u>LibreSilicon GitLab</u></p>
                   </a>
               </div>
                <div class="col-lg-3 col-md-3 text-center">
                    <a rel="noreferrer" href="https://wiki.libresilicon.com" target="_blank">
                        <img width="120px" style="margin-top: 20px;" src="img/wikimedia-logo.png">
                        <p><u>LibreSilicon Wiki</u></p>
                   </a>
               </div>
            </div>
        </div>
    </section>

    <section id="whitepaper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
	 	    <h2 class="section-heading">{{additionalDocs}}</h2>
                </div>
            </div>
	    <div class="row center">
                <div class="col-xs-4 text-center">
		    <a rel="noreferrer" href="files/propaganda/talk.pdf" target="_blank">
		    <img width="120px" style="margin-top: 20px;" src="img/LSA-pdf.svg">
		    <p><u>Lightning talk slides</u></p>
		    </a>
                </div>
                <div class="col-xs-4 text-center">
		    <a rel="noreferrer" href="https://www.youtube.com/watch?v=67rh6jB2UVQ#t=29m51s" target="_blank">
		    <img width="120px" style="margin-top: 20px;" src="img/LSA-video.svg">
		    <p><u>Lightning talk video</u></p>
		    </a>
                </div>
	    </div>
        </div>
    </section>

    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">{{contactUs}}</h2>
                </div>
            </div>
            <div class="row" style="margin-top: 30px;">
		    <div class="col-lg-3 col-md-3 text-center">
                        	<i class="fa fa-4x fa-envelope"></i>
			<p>
				<a href="mailto:contact@LibreSilicon.com"><br />contact@LibreSilicon.com</a><br />
			</p>
		    </div>
		    <div class="col-lg-3 col-md-3 text-center">
			<a rel="noreferrer" href="https://twitter.com/LibreSilicon" target="blank">
                        	<i class="fa fa-4x fa-twitter"></i>
				<p>
				<br />LibreSilicon</p>
			</a>
		    </div>
		    <!--<div class="col-lg-3 col-md-3 text-center">
			<a rel="noreferrer" href="https://matrix.libresilicon.com" target="blank">
                        	<i class="fa fa-4x fa-hashtag"></i>
				<p>
				<br />matrix.libresilicon.com
				<br />
				</p>
			</a>
		    </div>-->
		    <div class="col-lg-3 col-md-3 text-center">
                        	<i class="fa fa-4x fa-microphone"></i>
				<p>
				<br />mumble: murmur.libresilicon.com
				<br />#IC
				</p>
		    </div>
			<div class="col-lg-3 col-md-3 text-center">
	            <a rel="noreferrer" href="https://signal.group/#CjQKIABumTzquesrqcq0oIuGVMRaQnJ58EIzb1oklxGVfGA3EhAe5T6FEv40hSP2SeDR-wRa" target="blank">
                    <img width="60px" style="margin-top: 20px;" src="img/signal-logo.png">
		    <p>Signal</p>
		   </a>
		    </div>
            </div>
        </div>
    </section>

    <section id="sponsors">
        <div class="container text-center">
		<div class="row">
		<div class="col-lg-3 col-md-3 text-center"></div>
                <div class="col-lg-3 col-md-3 text-center">
			<img class="logo" src="img/lanceville.png" />
                </div>
                <div class="col-lg-3 col-md-3 text-center">
			<a rel="noreferrer" href="http://efabless.com" target="_blank"><img class="logo" style="margin: 50px auto auto 0px;" src="img/efabless.png" /></a>
			<a rel="noreferrer" href="http://pconas.de" target="_blank"><img class="logo" style="margin-top: 50px;" src="img/pconas.png" /></a>
                </div>
                </div>
		<div class="row">
		<div class="col-lg-3 col-md-3 text-center"></div>
                <div class="col-lg-6 col-md-6 text-center framed_container">
			<a rel="noreferrer" href="http://www.nff.ust.hk/en/home.html" target="_blank"><img class="logo" src="img/HKUST_Logo.png" /><img class="logo" src="img/NFF.jpg" /></a>
		</div>
		</div>
        </div>
<br/>
		<div class="col-lg-4 col-md-4 text-center"></div>
                <div class="col-lg-2 col-md-2 text-center">
			<a rel="noreferrer" href="http://opencircuitdesign.com" target="_blank"><img class="logo" src="img/opencircuitdesign.png" /><br />Open Circuit Design</a>
                </div>
		<div class="col-lg-2 col-md-2 text-center"><br />
			<a rel="noreferrer" href="http://www.clifford.at/yosys" target="_blank"><img class="logo" style="width: 300px;" src="img/yosys.png" /></a>
                </div>
        </div>
    </section>

	<div id="footer">
		<p>
		copyright
                <i class="fa fa-copyright text-primary"></i>
		Lanceville Technology 2023 
		</p>
	</div>

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Theme JavaScript -->
    <script src="js/creative.min.js"></script>

</body>

</html>
