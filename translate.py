#!/usr/bin/env python3.10
# -*- coding: utf-8 -*-

import os
from jinja2 import Environment, FileSystemLoader
from jinja2_markdown import MarkdownExtension
import json
import codecs
import markdown
from xml.etree.ElementTree import Element

PATH = os.path.dirname(os.path.abspath(__file__))

class DIVTableProcessor(markdown.blockprocessors.BlockProcessor):
    def __init__(self, parser):
        self.border = False
    def test(self, parent, block):
        is_divtable = block.startswith('[DIVTable]')
        return is_divtable
    def run(self, parent, blocks):
        block = blocks.pop(0)
        container_div = Element("div")
        container_div.attrib = {'class':'container'}
        for sb in block.split('\n')[1:]:
            el = Element("div")
            el.attrib = {'class':'col-xs-4 text-center'}
            el.text = markdown.markdown(sb)
            container_div.append(el)
        parent.append(container_div)

class DIVTable(markdown.extensions.Extension):
    def __init__(self, **kwargs):
        super(DIVTable, self).__init__(**kwargs)

    def extendMarkdown(self, md):
        md.parser.blockprocessors.register(DIVTableProcessor(md.parser), 'DIVTable', 75)

def get_json_content(name):
    filename="jsons/"+name+".json"
    try:
        json_content = json.load(fp=open(filename))
    except Exception as e:
        print("Error rendering: "+filename)
        print(e)
        return dict()
    else:
        return json_content

def custom_function(a):
    return a.replace('o', 'ay')

def render_template(jinja_env, name, content):
    template_filename=name+".tpl"
    template = jinja_env.get_template(template_filename)
    return template.render(content)

def get_md_content(pname, fname):
    mdroot = os.path.join(PATH, 'markdowns', pname)
    if fname in os.listdir(mdroot):
        mdfile = os.path.join(mdroot, fname)
        try:
            with open(mdfile, 'r') as f:
                tempMd = f.read()
                f.close()
        except:
            return {}
        else:
            md = markdown.Markdown(extensions=[DIVTable()])
            ret = md.convert(tempMd)
            md.reset()
            return ret

def get_news_content():
    news_dict = get_json_content("news")
    newsItems=[]
    for news in news_dict:
        newsItems.append({
            'title': news_dict[news],
            'description': get_md_content("news", news+".md")
        })
    content = {'newsItems': newsItems}
    return content

def create_html(jinja_env):
    content1 = get_json_content("index")
    content2 = get_news_content() 
    content = content1 | content2
    content['large_content'] = get_md_content("index", "large_content.md")

    with codecs.open("index.html", 'w', "utf-8-sig") as f:
        html = render_template(jinja_env, 'index', content)
        if(html is not None):
            f.write(html)

def create_rss(jinja_env):
    content = get_news_content() 
    with codecs.open("news.rss", 'w', "utf-8-sig") as f:
        html = render_template(jinja_env, 'news', content)
        if(html is not None):
            f.write(html)

def main():
    jinja_env = Environment(
        autoescape = False,
        loader=FileSystemLoader(os.path.join(PATH, 'templates')),
        trim_blocks = False,
        extensions=['jinja2_markdown.MarkdownExtension']
    )
    jinja_env.add_extension(MarkdownExtension)
    create_html(jinja_env)
    create_rss(jinja_env)

########################################

if __name__ == "__main__":
    main()


